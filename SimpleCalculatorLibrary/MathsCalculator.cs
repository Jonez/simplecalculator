﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace MathsCalculatorNamespace
{
    public class StringCalculator
    {
        public int Add(String rawNumbers)
        {
            if(rawNumbers == "") return 0;

            string[] customSeperator = new string[] { null };

            if (rawNumbers.StartsWith("//"))
            {
                rawNumbers = rawNumbers.Substring(2);
                if (!rawNumbers.Contains("[")) customSeperator = new string[] { rawNumbers.Substring(0, 1) };
                else
                {
                    customSeperator = Regex.Matches(rawNumbers, @"(?<=\[).+?(?=\])").Cast<Match>().Select(m => m.Value).ToArray();
                    int stringFrom = rawNumbers.IndexOf("[") + "[".Length;
                    int stringTo = rawNumbers.LastIndexOf("]");
                    rawNumbers = rawNumbers.Substring(stringTo + 1, rawNumbers.Length - stringTo - 1);
                }
            }

            string[] seperators = new string[] { "\n", "," };
            string[] allSeperators = seperators.Concat(customSeperator).ToArray();
            int[] convertedNums = Array.ConvertAll(rawNumbers.Split(allSeperators, StringSplitOptions.None).Where(x => !string.IsNullOrEmpty(x)).ToArray(), int.Parse);

            convertedNums = convertedNums.Where(i => i < 1000).ToArray(); //Removes any numbers bigger than 1000

            int[] negativeNumbers = convertedNums.Where(i => i < 0).ToArray();
            if (negativeNumbers.Length > 0) {
                string exceptionNumbers = "";
                foreach (var n in negativeNumbers) exceptionNumbers += n.ToString() + " ";
                throw new Exception(message: $"negatives not allowed: {exceptionNumbers}");
            }

            var total = convertedNums.Sum();
            return total;
        }
    }
}
