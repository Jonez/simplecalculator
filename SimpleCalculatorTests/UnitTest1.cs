﻿using Xunit;
using MathsCalculatorNamespace;

namespace SimpleCalculatorTests
{
    public class UnitTest1
    {
        [Fact]
        public void NoValueTest()
        {
            var maths = new StringCalculator();
            Assert.Equal(0, maths.Add(""));
        }

        [Fact]
        public void SingleNumberTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("4");
            Assert.Equal(4, result);
        }
        [Fact]
        public void TwoNumbersTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("1,2");
            Assert.Equal(3, result);
        }
        [Fact]
        public void MultipleNumbersTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("1,2,3,4,5,6,7,8,9");
            Assert.Equal(45, result);
        }
        [Fact]
        public void NewLineTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("1\n2,3");
            Assert.Equal(6, result);
        }
        [Fact]
        public void CustomSeparatorsTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("//;\n1;2");
            Assert.Equal(3, result);
        }

        [Fact]
        public void DisallowNegativesTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("1,-2,-3");
            Assert.Equal(3, result);
        }

        [Fact]
        public void IgnoreBiggerThanThousandTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("1001, 2");
            Assert.Equal(2, result);
        }

        [Fact]
        public void ArbitraryLengthSeparatorsTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("//[***]\n1***2***3");
            Assert.Equal(6, result);
        }

        [Fact]
        public void MultipleSingleLengthSeparatorsTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("//[*][%]\n1*2%3");
            Assert.Equal(6, result);
        }

        [Fact]
        public void MultipleCustomSeparatorsTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("//[foo][bar]\n1foo2bar3");
            Assert.Equal(6, result);
        }

        [Fact]
        public void MyOwnTestTest()
        {
            StringCalculator maths = new StringCalculator();
            int result = maths.Add("//[x][y][Matthew][Kenneth][Jones]\n1x2y3Matthew2Kenneth4Jones9");
            Assert.Equal(21, result);
        }
    }
}
